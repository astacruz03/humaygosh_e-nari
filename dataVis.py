# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 21:11:14 2019

@author: Audie
"""

import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from mpl_toolkits.mplot3d import Axes3D, proj3d

#import sklearn
#%% load data
f = 'H4Rice2019\\geno_pheno_train.csv'
tr_all = pd.read_csv(f)
tr_allc1 = tr_all.copy() 

#%% Economy SVD
tr_allc1 = tr_allc1.dropna(subset=['group'],axis=0)
#%%
phen = ['ID','group','GRWT100','GRWD','GRLT','LWD','Seedling_height']
gen = tr_allc1.drop(phen,axis=1)

#%% visualize labels
label_names = set(tr_allc1['group'])
labeln = ['admix','aro','aus','ind1A','ind1B','ind2','ind3','indx','japx','temp','trop1','trop2']
markers = ['o','^','*','o','^','*','s','d','x','+','o','^']
colors = ['r','g','c','b','b','b','b','b','m','k','y','y']
lnd = {}
j = 0
#for i in label_names:
for i in labeln:
    lnd[i] = {'m':markers[j],'c':colors[j]}
    j = j+1

#%%
#U,s,V = np.linalg.svd(gen)
#tr_allc1.to_csv('VarData.csv',index=False)    
    
pca = PCA(n_components=3)
pcaComp = pca.fit_transform(gen)
pcaDF = pd.DataFrame(data=pcaComp,columns=['PCA 1','PCA 2','PCA 3'])
finaldf = pd.concat([pcaDF,tr_allc1[['group']]],axis=1)
pcaDF.to_csv('PCA_varieties_feat.csv',header = False,index =False)

tr_allc1['group'].to_excel('PCA_varieties_labels.xlsx',header = False,index =False)
finaldf.to_csv('PCA_varieties.csv',header = False,index =False)
finaldf.to_csv('PCA_varieties1.csv',index =False)
#%% plot
fig = plt.figure()
ax = fig.add_subplot(111,projection = '3d')
ax.set_xlabel('PCA 1')
ax.set_ylabel('PCA 2')
ax.set_zlabel('PCA 3')
#ax.set_title('Varieties')
ax.hold(True)
for label in lnd:
    indicesToKeep = finaldf['group'] == label 
    ax.scatter(finaldf.loc[indicesToKeep, 'PCA 1'],finaldf.loc[indicesToKeep, 'PCA 2'],finaldf.loc[indicesToKeep, 'PCA 3'],\
               c = lnd[label]['c'],marker=lnd[label]['m'], s=20)

ax.legend(list(lnd.keys()))
ax.grid()
for angle in range(0, 360):
    ax.view_init(30, angle)
    plt.draw()
    plt.pause(.001)
ax.hold(False)
#%% get phenotype metadata of groups: population
N_group = tr_allc1['group'].value_counts()
figN = plt.figure()
axN = figN.add_subplot(111)
labels = N_group.index.to_list() 
color_b = []
for i in labels:
    color_b.append(lnd[i]['c'])
x = N_group.values
ypos = np.arange(len(labels))
axN.barh(ypos,x,0.5,align='center',color=color_b)
axN.set_yticks(ypos)
axN.set_yticklabels(labels)
axN.invert_yaxis()
axN.set_xlabel('Count')

x2 = np.cumsum(x)/np.sum(x)*100
axN2 = axN.twiny()
axN2.plot(x2,ypos,'k-.',marker='o')
axN2.set_xlabel('Percentage population')
#%% get phenotype metadata of groups: grwt100
df_phen = tr_allc1[['group','GRWT100','GRWD','GRLT','LWD','Seedling_height']] 
stat = df_phen.describe()

#%% get phenotype metadata of groups: 
phen_n = ['GRWT100','GRWD','GRLT','LWD','Seedling_height']
phend = {}
for i in phen_n:
    meanit = df_phen.groupby('group')[i].mean().reset_index() 
    stdit = df_phen.groupby('group')[i].std().reset_index()
    phend[i] = {'avg':meanit.set_index('group'),\
         'std':stdit.set_index('group')}
#%% get PCA mode 1 and 2 and project the grain yield
phenW = 'GRWT100'
legn = []
for l in lnd:
    legn += [l+' '+'avg: %.2f std: %.2f' %(phend[phenW]['avg'].loc[l],phend[phenW]['std'].loc[l])]
fig = plt.figure(figsize=(19,9.2))
ax = fig.add_subplot(111,projection = '3d')
ax.set_title('Clustering of '+phenW)
ax.set_xlabel('PCA 1')
ax.set_ylabel('PCA 2')
ax.set_zlabel(phenW)
#ax.set_title('Varieties')
ax.hold(True)
axpl = []
for label in lnd:
    indicesToKeep = finaldf['group'] == label
    if label in ['japx','temp']:
        fc = lnd[label]['c']
    else:
        fc = 'none'
    x = finaldf.loc[indicesToKeep, 'PCA 1']
    y = finaldf.loc[indicesToKeep, 'PCA 2']
    z = tr_allc1.loc[indicesToKeep ,phenW]
    axpl += [ax.scatter(x,y,z,\
           color = lnd[label]['c'], marker=lnd[label]['m'],facecolor = fc,edgecolor = lnd[label]['c'], s=20)]
    
    pca1_avg = np.sum(finaldf.loc[indicesToKeep,'PCA 1'])/len(finaldf.loc[indicesToKeep,'PCA 1'])
    pca2_avg = np.sum(finaldf.loc[indicesToKeep,'PCA 2'])/len(finaldf.loc[indicesToKeep,'PCA 2'])
    ax.scatter(pca1_avg,pca2_avg,phend[phenW]['avg'].loc[label],color = lnd[label]['c'], marker=lnd[label]['m'],facecolor = lnd[label]['c'],edgecolor = 'k', s=20*3)

f = lambda x,y,z: proj3d.proj_transform(x,y,z, ax.get_proj())[:2]    
#ax.legend(list(lnd.keys()))
ax.legend(tuple(axpl),tuple(legn),ncol=4,framealpha=0.1,loc='lower left',bbox_to_anchor=f(-50,0,-3),bbox_transform=ax.transData)
ax.grid()
for angle in range(0, 360):
    ax.view_init(30, angle)
    plt.draw()
    plt.pause(.001)
ax.hold(False)
#%% get PCA mode 1 and 2 and project the grain width
phenW = 'GRWD'
legn = []
for l in lnd:
    legn += [l+' '+'avg: %.2f std: %.2f' %(phend[phenW]['avg'].loc[l],phend[phenW]['std'].loc[l])]
fig = plt.figure(figsize=(19,9.2))
ax = fig.add_subplot(111,projection = '3d')
ax.set_title('Clustering of '+phenW)
ax.set_xlabel('PCA 1')
ax.set_ylabel('PCA 2')
ax.set_zlabel(phenW)
#ax.set_title('Varieties')
ax.hold(True)
axpl = []
for label in lnd:
    indicesToKeep = finaldf['group'] == label
    if label in ['japx','temp']:
        fc = lnd[label]['c']
    else:
        fc = 'none'
    x = finaldf.loc[indicesToKeep, 'PCA 1']
    y = finaldf.loc[indicesToKeep, 'PCA 2']
    z = tr_allc1.loc[indicesToKeep ,phenW]
    axpl += [ax.scatter(x,y,z,\
           color = lnd[label]['c'], marker=lnd[label]['m'],facecolor = fc,edgecolor = lnd[label]['c'], s=20)]
    
    pca1_avg = np.sum(finaldf.loc[indicesToKeep,'PCA 1'])/len(finaldf.loc[indicesToKeep,'PCA 1'])
    pca2_avg = np.sum(finaldf.loc[indicesToKeep,'PCA 2'])/len(finaldf.loc[indicesToKeep,'PCA 2'])
    ax.scatter(pca1_avg,pca2_avg,phend[phenW]['avg'].loc[label],color = lnd[label]['c'], marker=lnd[label]['m'],facecolor = lnd[label]['c'],edgecolor = 'k', s=20*3)

f = lambda x,y,z: proj3d.proj_transform(x,y,z, ax.get_proj())[:2]    
#ax.legend(list(lnd.keys()))
ax.legend(tuple(axpl),tuple(legn),ncol=4,framealpha=0.1,loc='lower left',bbox_to_anchor=f(-50,0,-3),bbox_transform=ax.transData)
ax.grid()
for angle in range(0, 360):
    ax.view_init(30, angle)
    plt.draw()
    plt.pause(.001)
ax.hold(False)
#%% get PCA mode 1 and 2 and project the grain length
phenW = 'GRLT'
legn = []
for l in lnd:
    legn += [l+' '+'avg: %.2f std: %.2f' %(phend[phenW]['avg'].loc[l],phend[phenW]['std'].loc[l])]
fig = plt.figure(figsize=(19,9.2))
ax = fig.add_subplot(111,projection = '3d')
ax.set_title('Clustering of '+phenW)
ax.set_xlabel('PCA 1')
ax.set_ylabel('PCA 2')
ax.set_zlabel(phenW)
#ax.set_title('Varieties')
ax.hold(True)
axpl = []
for label in lnd:
    indicesToKeep = finaldf['group'] == label
    if label in ['japx','temp']:
        fc = lnd[label]['c']
    else:
        fc = 'none'
    x = finaldf.loc[indicesToKeep, 'PCA 1']
    y = finaldf.loc[indicesToKeep, 'PCA 2']
    z = tr_allc1.loc[indicesToKeep ,phenW]
    axpl += [ax.scatter(x,y,z,\
           color = lnd[label]['c'], marker=lnd[label]['m'],facecolor = fc,edgecolor = lnd[label]['c'], s=20)]
    
    pca1_avg = np.sum(finaldf.loc[indicesToKeep,'PCA 1'])/len(finaldf.loc[indicesToKeep,'PCA 1'])
    pca2_avg = np.sum(finaldf.loc[indicesToKeep,'PCA 2'])/len(finaldf.loc[indicesToKeep,'PCA 2'])
    ax.scatter(pca1_avg,pca2_avg,phend[phenW]['avg'].loc[label],color = lnd[label]['c'], marker=lnd[label]['m'],facecolor = lnd[label]['c'],edgecolor = 'k', s=20*3)

f = lambda x,y,z: proj3d.proj_transform(x,y,z, ax.get_proj())[:2]    
#ax.legend(list(lnd.keys()))
ax.legend(tuple(axpl),tuple(legn),ncol=4,framealpha=0.1,loc='lower left',bbox_to_anchor=f(-50,0,-3),bbox_transform=ax.transData)
ax.grid()
for angle in range(0, 360):
    ax.view_init(30, angle)
    plt.draw()
    plt.pause(.001)
ax.hold(False)
#%% get PCA mode 1 and 2 and project the grain length
phenW = 'GRLT'
legn = []
for l in lnd:
    legn += [l+' '+'avg: %.2f std: %.2f' %(phend[phenW]['avg'].loc[l],phend[phenW]['std'].loc[l])]
fig = plt.figure(figsize=(19,9.2))
ax = fig.add_subplot(111,projection = '3d')
ax.set_title('Clustering of '+phenW)
ax.set_xlabel('PCA 1')
ax.set_ylabel('PCA 2')
ax.set_zlabel(phenW)
#ax.set_title('Varieties')
ax.hold(True)
axpl = []
for label in lnd:
    indicesToKeep = finaldf['group'] == label
    if label in ['japx','temp']:
        fc = lnd[label]['c']
    else:
        fc = 'none'
    x = finaldf.loc[indicesToKeep, 'PCA 1']
    y = finaldf.loc[indicesToKeep, 'PCA 2']
    z = tr_allc1.loc[indicesToKeep ,phenW]
    axpl += [ax.scatter(x,y,z,\
           color = lnd[label]['c'], marker=lnd[label]['m'],facecolor = fc,edgecolor = lnd[label]['c'], s=20)]
    
    pca1_avg = np.sum(finaldf.loc[indicesToKeep,'PCA 1'])/len(finaldf.loc[indicesToKeep,'PCA 1'])
    pca2_avg = np.sum(finaldf.loc[indicesToKeep,'PCA 2'])/len(finaldf.loc[indicesToKeep,'PCA 2'])
    ax.scatter(pca1_avg,pca2_avg,phend[phenW]['avg'].loc[label],color = lnd[label]['c'], marker=lnd[label]['m'],facecolor = lnd[label]['c'],edgecolor = 'k', s=20*3)

f = lambda x,y,z: proj3d.proj_transform(x,y,z, ax.get_proj())[:2]    
#ax.legend(list(lnd.keys()))
ax.legend(tuple(axpl),tuple(legn),ncol=4,framealpha=0.1,loc='lower left',bbox_to_anchor=f(-50,0,-3),bbox_transform=ax.transData)
ax.grid()
for angle in range(0, 360):
    ax.view_init(30, angle)
    plt.draw()
    plt.pause(.001)
ax.hold(False)
#%% get PCA mode 1 and 2 and project the seedling height
phenW = 'Seedling_height'
legn = []
for l in lnd:
    legn += [l+' '+'avg: %.2f std: %.2f' %(phend[phenW]['avg'].loc[l],phend[phenW]['std'].loc[l])]
fig = plt.figure(figsize=(19,9.2))
ax = fig.add_subplot(111,projection = '3d')
ax.set_title('Clustering of '+phenW)
ax.set_xlabel('PCA 1')
ax.set_ylabel('PCA 2')
ax.set_zlabel(phenW)
#ax.set_title('Varieties')
ax.hold(True)
axpl = []
for label in lnd:
    indicesToKeep = finaldf['group'] == label
    if label in ['japx','temp']:
        fc = lnd[label]['c']
    else:
        fc = 'none'
    x = finaldf.loc[indicesToKeep, 'PCA 1']
    y = finaldf.loc[indicesToKeep, 'PCA 2']
    z = tr_allc1.loc[indicesToKeep , phenW]
    axpl += [ax.scatter(x,y,z,\
           color = lnd[label]['c'], marker=lnd[label]['m'],facecolor = fc,edgecolor = lnd[label]['c'], s=20)]
    
    pca1_avg = np.sum(finaldf.loc[indicesToKeep,'PCA 1'])/len(finaldf.loc[indicesToKeep,'PCA 1'])
    pca2_avg = np.sum(finaldf.loc[indicesToKeep,'PCA 2'])/len(finaldf.loc[indicesToKeep,'PCA 2'])
    ax.scatter(pca1_avg,pca2_avg,phend[phenW]['avg'].loc[label],color = lnd[label]['c'], marker=lnd[label]['m'],facecolor = lnd[label]['c'],edgecolor = 'k', s=20*3)

f = lambda x,y,z: proj3d.proj_transform(x,y,z, ax.get_proj())[:2]    
#ax.legend(list(lnd.keys()))
ax.legend(tuple(axpl),tuple(legn),ncol=4,framealpha=0.1,loc='lower left',bbox_to_anchor=f(-50,0,-3),bbox_transform=ax.transData)
ax.grid()
for angle in range(0, 360):
    ax.view_init(30, angle)
    plt.draw()
    plt.pause(.001)
ax.hold(False)